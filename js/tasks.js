
const validateCredentials = () => {
    const token = localStorage.getItem('token')
    if(token != null){
        $('#token').val(token)
    }else{
        location.href='login.html'
    }
}

validateCredentials()

const getAllTasks = () => {

    const token = $('#token').val()

    $.ajax({
        method: 'GET',
        headers: { 'Access-Control-Allow-Headers': 'Content-Type', 'Authorization': token },
        url: 'http://localhost:3000/tasks',
        success (res) {
            if(res.body.length == 0){
                $('.tasks').html(`<div class="alert alert-primary" role="alert">
                    It's very easy to add a new task!
                </div>`)
            }else{
                $('.tasks').html(`
                <table class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <td>#</td>
                        <td>Title</td>
                        <td>Description</td>
                        <td>Status</td>
                        <td>Date created</td>
                        <td>Date updated</td>
                        <td>Change Status</td>
                        <td>Delete</td>
                    </tr>
                    </thead>
                    <tbody class="tasks-list">
                    
                    </tbody>
                </table>
                `)
                let list = ''
                $.each(res.body, (key, value) => {

                    const id = value.id;
                    const title = value.title;
                    const description = value.description;
                    let status = value.status_id;
                    const createdAt = value.created_at;
                    const updatedAt = value.updated_at;
                    let buttonStatusClass = 'Success'

                    switch (status) {
                        case 1:
                            statusIcon = '<i class="fas fa-bell"></i>'
                            buttonStatusClass = 'btn-warning'
                            break;
                        case 2:
                            statusIcon = '<i class="fas fa-spinner"></i>'
                            buttonStatusClass = 'btn-primary'
                            break;
                        case 3:
                            statusIcon = '<i class="fas fa-check"></i>'
                            buttonStatusClass = 'btn-success'
                            break;
                        default:
                            break;
                    }

                    if(status != 4){
                        list += (`
                        <tr>
                            <td>${id}</td>
                            <td>${title}</td>
                            <td>${description}</td>
                            <td>${status}</td>
                            <td>${createdAt}</td>
                            <td>${updatedAt}</td>
                            <td><button onclick="(changeStatus(${id}, ${status}))" class="btn ${buttonStatusClass}">${statusIcon}</button></td>
                            <td><button onclick="(deleteTask(${id}))" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button></td>
                        </tr>`
                        )
                    }

                });
                $('.tasks-list').html(list)
            }
            
        }
    })
}

getAllTasks()

const addTask = () => {
    const title = $('#taskTitle').val()
    let description = $('#taskDescription').val()

    if(!description)
        description = ''    

    if(!title){
        $('#taskTitle').css('border-color', 'red')
    }else{
        const dataObject = {
            data:{
                title:title,
                description:description
            }
        }
        const token = $('#token').val()
        $.ajax({
            method: 'POST',
            url: 'http://localhost:3000/tasks',
            headers: { 'Access-Control-Allow-Headers': 'Content-Type', 'Authorization': token },
            data: dataObject,
            dataType: 'json',
            success (res){
                $('#taskTitle').val('')
                $('#taskDescription').val('')
                getAllTasks()
            }
        })
    }


}

const changeStatus = (id, status) => {
    
    switch (status) {
        case 1:
            status = 2
            break;
        case 2:
            status = 3
            break;
        case 3:
            status = 1
            break;
        default:
            break;
    }

    const dataObject = {
        data:{
            id:id,
            status:status
        }
    }
    const token = $('#token').val()
    $.ajax({
        method: 'PUT',
        url: 'http://localhost:3000/tasks',
        headers: 
        { 'Access-Control-Allow-Headers': 'Content-Type', 'Authorization': token },
        data: dataObject,
        dataType: 'json',
        success (res) {
            getAllTasks()
        }
    })
}

const deleteTask = (id) => {
    
    const dataObject = {
        data:{
            id:id
        }
    }

    const token = $('#token').val()

    $.ajax({
        method: 'DELETE',
        url: 'http://localhost:3000/tasks',
        headers: 
        { 'Access-Control-Allow-Headers': 'Content-Type', 'Authorization': token },
        data: dataObject,
        dataType: 'json',
        success (res) {
            getAllTasks()
        }
    })
}

const getReports = () => {

    const token = $('#token').val()

    $.ajax({
        method: 'GET',
        headers: { 'Access-Control-Allow-Headers': 'Content-Type', 'Authorization': token },
        url: 'http://localhost:3000/tasks',
        success (res) {
            const totalTasks = res.body.length
            if(totalTasks == 0){

                $('.totalTasks').html('0')
                $('.completedTime').html('No data')
                $('.deletedTasks').html('No data')

            }else{
                
                $('.totalTasks').html(res.body.length)

                let countCompleted = 0
                let countDeleted = 0
                let dates = []

                $.each(res.body, (key, value) => {

                    if(value.status_id == 3){
                        countCompleted++
                        const creadtedDate = value.created_at
                        const updatedDate = value.updated_at

                        dates.push({"dateIn":creadtedDate, "dateOut":updatedDate, "differences": averageTime(creadtedDate, updatedDate)})
                    }

                    if(value.status_id == 4)
                        countDeleted++
                        
                })

                let percentTime = 0

                if(countCompleted == 0){
                    $('.completedTime').html('No completed tasks')
                }else{
                    console.log(dates[0].differences)
                    timeDifference = []
                    $.each(dates, (key, value) => {
                        timeDifference.push(value.differences.minutes)
                    })
                    const sumTotal = timeDifference.reduce((partialSum, a) => partialSum + a, 0);
                    percentTime = sumTotal/timeDifference.length
                    $('.completedTime').html(`${percentTime.toFixed(2)} minutes`)
                }

                let percentDeleted = 0

                if(countDeleted == 0){
                    $('.deletedTasks').html('No deleted tasks')
                }else{
                    percentDeleted = (countDeleted/totalTasks)*100
                    $('.deletedTasks').html(`${percentDeleted.toFixed(2)}%`)
                }

            }
        }
    })
}


const logout = () => {
    localStorage.removeItem('token')
    location.href = 'login.html'
}

const removeRedBorder = () => {
    $('#taskTitle').css('border-color', 'transparent')
}

const averageTime = (date1, date2) => {
    let difference = new Date(date1).getTime() - new Date(date2).getTime();

    const daysDifference = Math.floor(difference/1000/60/60/24);
    difference -= daysDifference*1000*60*60*24

    const hoursDifference = Math.floor(difference/1000/60/60);
    difference -= hoursDifference*1000*60*60

    const minutesDifference = Math.floor(difference/1000/60);
    difference -= minutesDifference*1000*60

    const secondsDifference = Math.floor(difference/1000);

    return {
        days: daysDifference,
        hours: hoursDifference,
        minutes: minutesDifference,
        seconds: secondsDifference
    }
}