const login = () => {

    const email = $('#email').val()
    const password = $('#password').val()

    if(!email){
        $('#email').css('border-color', 'red')
    }else if(!password){
        $('#password').css('border-color', 'red')
    }else{

        const dataObject = {
            data:{
                email:email,
                password:password
            }
        }

        $.ajax({
            method:'POST',
            url: 'http://localhost:3000/login',
            data: dataObject,
            dataType: 'json',
            success (res){

                if(res.statusCode == 401){
                    $('.message').html('Oops! Wrong credentials')
                    localStorage.removeItem('token')
                }else{
                    localStorage.setItem('token', res.body.token)
                    location.href = 'index.html'
                }

            }
        })
    }


}